set(DOCUMENTATION "Point Matching based Image Co-registration application.")

otb_module(OTBAppPointMatchCoregistration 
  DEPENDS
    OTBGdalAdapters
    OTBDescriptors
    OTBTransform
    OTBApplicationEngine
    OTBImageBase
    OTBGDAL
    OTBImageManipulation
    OTBProjection
    OTBCarto
    OTBMathParser
    OTBCommon
    OTBInterpolation
    OTBITK
  TEST_DEPENDS
    OTBTestKernel
    OTBCommandLine
  
  DESCRIPTION
    "${DOCUMENTATION}"
  )
